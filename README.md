# Data sets:

This is a repo with links to interesting and helpful datasets:


# Post codes

1. This [link](https://www.doogal.co.uk/postcodedownloads.php) contains postcode data for UK mostly in CSV format:
    
    - postcodes.csv 1.18GB

# NLP

## Chatbot

1. [GitHubRepo](https://github.com/PolyAI-LDN/conversational-datasets)

## Emotion Detection

1. This is from [Kaggle sentiment140](https://www.kaggle.com/kazanova/sentiment140) dataset. It contains 1.6M tweets extracted from twitter 

2. The dataset for this competition contains text that may be considered profane, vulgar, or offensive. Kaggle [link](https://www.kaggle.com/c/jigsaw-unintended-bias-in-toxicity-classification/data)

    - jigsaw-unintended-bias-in-toxicity-classification